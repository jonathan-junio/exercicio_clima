public abstract class RespostaAbstrata
{
    private String titulo;
    private String texto;

    public RespostaAbstrata (String titulo, String texto)
    {
        this.titulo = titulo;
        this.texto = texto;
    }

    public String getTitulo ()
    {
        return this.titulo;
    }

    public String getTexto ()
    {
        return this.texto;
    }

    @Override
    public String toString()
    {
        String str = "Titulo: " + this.getTitulo() + "\nTexto: " + this.getTexto();
        return str;
    }
}