public class RespostaFactory
{
    public static Resposta getResposta(String tipo, String titulo, String texto)
    {
        if (tipo == "alta")
        {
            return new Processo(titulo, texto);
        } else if (tipo ==  "baixa_email")
        {
            return new Email(titulo, texto);
        } else if (tipo == "baixa_sms")
        {
            return new Sms(titulo, texto);
        } else {
            return null;
        }
    }
}