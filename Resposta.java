interface Resposta
{
    String getTitulo();
    String getTexto();
    String toString();

}