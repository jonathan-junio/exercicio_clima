public class Teste
{
  public static void main(String[] args)
  {
    Resposta email = RespostaFactory.getResposta("baixa_email", "teste email", "teste titulo");
    System.out.println(email.toString());

    Resposta sms = RespostaFactory.getResposta("baixa_sms", "teste sms", "teste titulo 2");
    System.out.println(sms.toString());

    Resposta proc = RespostaFactory.getResposta("alta", "teste processo", "teste titulo 3");
    System.out.println(proc.toString());
  }
}